This module adds its own validate function to webforms. It removes the original validate functions
and attaches its own to each webform. It returns field names in the error messages which are not
available in the current webform module.

It uses Drupals own email validation which is very good.

It is easily extended to validate fields in a more specific way than is available from webforms.
Extend the validate_this_webform($form_id, $form_values) function.

If you add something useful and its useable across sites, make sure to add it to this module.